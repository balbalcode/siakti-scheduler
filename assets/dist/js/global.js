function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
$(document).ready(function () {
	$('#datatable, .datatable').DataTable({})

	$('.del-dialog').click(function () {
		var parent = this.dataset.id
		var route = this.dataset.route
		var redirect = this.dataset.redirect
		var title = "Hapus Data";
		var desc = "Apakah anda ingin menghapus data ini?";
		var confirm = "Data berhasil dihapus";

		Swal.fire({
			title: title,
			text: desc,
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya, Hapus data!'
		}).then((result) => {
			if (result.value) {
				axios.get(route).then(function (response) {
					Swal.fire(
						'Success',
						confirm,
						'success'
					)
					window.setTimeout(function () { window.location.href=redirect }, 2000);
				}).catch(function (error) {
					Swal.fire("Oops", "We couldn't connect to the server!", "error");
				})
				
			}
		})
	});
	

	if (getParameterByName("inp") == "success") {
		Swal.fire(
			'Success',
			'Success to add data',
			'success'
		)
	}
	if (getParameterByName("inp") == "fail") {
		Swal.fire(
			'Failed',
			'Failed to add data',
			'error'
		)
	}
	if (getParameterByName("upd") == "success") {
		Swal.fire(
			'Success',
			'Success to update data',
			'success'
		)
	}
	if (getParameterByName("upd") == "fail") {
		Swal.fire(
			'Failed',
			'Failed to update data',
			'error'
		)
	}
	if (getParameterByName("del") == "success") {
		Swal.fire(
			'Success',
			'Success to delete data',
			'success'
		)
	}
	if (getParameterByName("del") == "fail") {
		Swal.fire(
			'Failed',
			'Failed to delete data',
			'error'
		)
	}


});