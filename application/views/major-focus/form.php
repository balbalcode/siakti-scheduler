
<div class="content-header">

</div>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Prodi</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="mx-auto col-10 col-md-9">
                        <form role="form" action="<?=base_url()?>focus/save" method="post">
                            <div class="box-body">

                                <input type="hidden" name="prodi_id" value="<?=@$data[0]->prodi_id?>">
                                <?=input_text_group("namaprod",@$data[0]->namaprod,"Nama Prodi" ,"Masukkan Nama")?>
                                <?php echo form_error('namaprod'); ?>
                                <?=input_text_group("jenprod",@$data[0]->jenprod,"Jenis" ,"Jenis")?>
                                <?php echo form_error('jenprod'); ?>
                                <?=select_join_group($jurusanList->responseData,@$data[0]->jurusan_kodejur,"namajur" ,"kodejur","Jurusan")?>
                                <?php echo form_error('namajur'); ?>
                            </div>
                            <div class="box-footer">
                                <button class="btn btn-primary btn-sm" type="submit">
                                    <i class="fa fa-save"></i> Simpan
                                </button>
                                <a class="btn btn-dark btn-sm" href="<?=base_url().getController()?>">
                                    <i class="fa fa-trash"></i> Batal
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>