
<div class="content-header">

</div>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Prodi</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 pb-2">
                        <div class="float-right">
                            <a class="btn btn-primary btn-sm"  href="<?=base_url().getController()?>/form">
                                <i class="fa fa-plus"></i>  Buat Prodi baru
                            </a>
                        </div>
                        <div class="clearboth"></div>
                    </div>
                    <div class="col-md-12">

                        <table id="datatable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tipe</th>
                                    <th>Nama</th>
                                    <th width="15%">Option</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                if (@$data->responseData ) {
                                    
                                    foreach ($data->responseData as $key => $value) { ?>
                                        <tr>
                                            <td><?= $no ?></td>
                                            <td><?= $value->namaprod ?></td>
                                            <td><?= $value->jenprod   ?></td>
                                            <td>
                                                <a href="<?= base_url() . getController() ?>/form/<?= $value->prodi_id ?>" class="btn btn-primary btn-xs">
                                                    <i class="fa fa-edit"></i> Sunting
                                                </a>
                                                <button class="btn btn-danger btn-xs del-dialog" data-id="<?=$value->prodi_id?>" data-route="<?=base_url().getController().'/delete/'.$value->prodi_id?>" data-redirect="<?=base_url().getController()?>">
                                                    <i class="fa fa-times"></i> Hapus
                                                </button>
                                            </td>
                                        </tr>
                                        <?php $no++;
                                    }
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

</script>