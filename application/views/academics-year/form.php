
<div class="content-header">

</div>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Tahun Akademik</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="mx-auto col-10 col-md-9">
                        <form role="form" action="<?=base_url().getController()?>/save" method="post">
                            <div class="box-body">

                                <input type="hidden" name="oldId" value="<?=@$data[0]->thn_akad_id?>">
                                <?=input_text_group("thn_akad_id",@$data[0]->thn_akad_id,"Id Akademik" ,"Id Akademik")?>
                                <?php echo form_error('thn_akad_id'); ?>
                                <?=input_text_group("tahun_akad",@$data[0]->tahun_akad,"Tahun Akademik" ,"Tahun Akademik")?>
                                <?php echo form_error('tahun_akad'); ?>
                                <?=select_join_group($semesterList->responseData,@$data[0]->smester_semester_nm,"semester_nm" ,"semester_nm","Semester")?>
                                <?php echo form_error('semester_nm'); ?>
                            </div>
                            <div class="box-footer">
                                <button class="btn btn-primary btn-sm" type="submit">
                                    <i class="fa fa-save"></i> Simpan
                                </button>
                                <a class="btn btn-dark btn-sm" href="<?=base_url().getController()?>">
                                    <i class="fa fa-trash"></i> Batal
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>