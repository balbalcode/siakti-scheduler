
<div class="content-header">

</div>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Kelas</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="mx-auto col-10 col-md-9">
                        <form role="form" action="<?=base_url()?>classes/save" method="post">
                            <div class="box-body">
                                <input type="hidden" name="namakur" value="<?=@$data[0]->kodeklas?>">
                                <?=input_text_group("kodeklas",@$data[0]->kodeklas,"Kode Class" ,"Masukkan Kode")?>
                                <?php echo form_error('kodeklas'); ?>
                                <?=input_text_group("namaklas",@$data[0]->namaklas,"Nama Kelas " ,"Masukkan Nama")?>
                                <?php echo form_error('namaklas'); ?>
                                <?=select_join_group($kelasList->responseData,@$data[0]->jns_kls_nama_jnskls,"nama_jnskls" ,"nama_jnskls","Jenis")?>
                                <?php echo form_error('nama_jnskls'); ?>
                                <?=select_join_group($prodiList->responseData,@$data[0]->prodi_prodi_id,"namaprod" ,"prodi_id","Prodi")?>
                                <?php echo form_error('prodi_id'); ?>
                            </div>
                            <div class="box-footer">
                                <button class="btn btn-primary btn-sm" type="submit">
                                    <i class="fa fa-save"></i> Save
                                </button>
                                <a class="btn btn-dark btn-sm" href="<?=base_url().getController()?>">
                                    <i class="fa fa-trash"></i> Canccel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>