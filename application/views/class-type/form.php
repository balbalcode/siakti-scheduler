
<div class="content-header">

</div>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Jenis Kelas</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="mx-auto col-10 col-md-9">
                        <form role="form" action="<?=base_url()?>RoomType/save" method="post">
                            <div class="box-body">
                                <input type="hidden" name="oldJns" value="<?=@$data[0]->nama_jnskls?>">
                                <?=input_text_group("nama_jnskls",@$data[0]->nama_jnskls,"Jenis Kelas" ,"Jenis Kelas")?>
                                <?php echo form_error('nama_jnskls'); ?>

                            </div>
                            <div class="box-footer">
                                <button class="btn btn-primary btn-sm" type="submit">
                                    <i class="fa fa-save"></i> Simpan
                                </button>
                                <a class="btn btn-dark btn-sm" href="<?=base_url().getController()?>">
                                    <i class="fa fa-trash"></i> Batal
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>