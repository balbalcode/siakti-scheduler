
<div class="content-header">

</div>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Course</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="mx-auto col-10 col-md-9">
                        <form role="form">
                            <div class="box-body">
                                <?=input_text_group("courseName","","Course Name" ,"Insert Name","true")?>
                                <?=input_text_group("courseStudy of Hour","","Course Study of Hour" ,"Insert Study of Hour","true")?>
                                <?=select_join_group($courseType,"","courseTypeName" ,"courseTypeId","Course Type")?>
                                <?=input_textarea_group("courseAddress","","Course Address" ,"Insert Address","true")?>
                            </div>

                            <div class="box-footer">
                                <a class="btn btn-primary btn-sm" href="<?=base_url().getController()?>">
                                    <i class="fa fa-save"></i> Save
                                </a>
                                <a class="btn btn-dark btn-sm" href="<?=base_url().getController()?>">
                                    <i class="fa fa-trash"></i> Canccel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

</script>