
<div class="content-header">

</div>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Academic</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="mx-auto col-10 col-md-9">
                        <form role="form">
                            <div class="box-body">
                                <?=input_text_group("lectureName","","Lecture Name" ,"Insert Name","true")?>
                                <?=input_text_group("lectureNIP","","Lecture NIP" ,"Insert NIP","true")?>
                                <?=input_textarea_group("lectureAddress","","Lecture Address" ,"Insert Address","true")?>
                                <?=input_text_group("lectureType","","Lecture Type" ,"Insert Type","true")?>
                                <?=input_text_group("lectureDuration","","Teaching Duration" ,"Insert Duration as Hour","true")?>

                                <div class="form-group">
                                    <label class="">Is Avail to Teaching</label>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="97%">Course</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr> 
                                                <td>
                                                    <select class="form-control search-select" >
                                                        <option>Matematika Diskrit</option>
                                                        <option>Pemograman Web</option>
                                                        <option>Database</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                </td>
                                            </tr>
                                            <tr> 
                                                <td>
                                                    <select class="form-control search-select" >
                                                        <option>Matematika Diskrit</option>
                                                        <option selected="">Pemograman Web</option>
                                                        <option>Database</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2">
                                                    <a href="#" class="btn btn-primary btn-sm btn-block">
                                                        Add New 
                                                    </a>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>

                            <div class="box-footer">
                                <a class="btn btn-primary btn-sm" href="<?=base_url().getController()?>">
                                    <i class="fa fa-save"></i> Save
                                </a>
                                <a class="btn btn-dark btn-sm" href="<?=base_url().getController()?>">
                                    <i class="fa fa-trash"></i> Canccel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

</script>