
<div class="content-header">

</div>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Academic</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="mx-auto col-10 col-md-9">
                        <form role="form">
                            <div class="box-body">
                                <?=input_text_group("daysName","","Days" ,"Insert Days","true")?>
                                <?=input_textarea_group("daysDetail","","Detail" ,"Insert Detail","true")?>

                                <div class="form-group">
                                    <label class="">Is Avail to Teaching</label>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="47%">Start Hour</th>
                                                <th width="47%">Finish Hour</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr> 
                                                <td>
                                                   <input type="text" name="" id="" placeholder="start hour" class="form-control"> 
                                                </td>
                                                <td>
                                                   <input type="text" name="" id="" placeholder="finish hour" class="form-control"> 
                                                </td>
                                                <td>
                                                    <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                </td>
                                            </tr>
                                            <tr> 
                                                <td>
                                                   <input type="text" name="" id="" placeholder="start hour" class="form-control"> 
                                                </td>
                                                <td>
                                                   <input type="text" name="" id="" placeholder="finish hour" class="form-control"> 
                                                </td>
                                                <td>
                                                    <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                </td>
                                            </tr>
                                            <tr> 
                                                <td>
                                                   <input type="text" name="" id="" placeholder="start hour" class="form-control"> 
                                                </td>
                                                <td>
                                                   <input type="text" name="" id="" placeholder="finish hour" class="form-control"> 
                                                </td>
                                                <td>
                                                    <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                </td>
                                            </tr>
                                            <tr> 
                                                <td>
                                                   <input type="text" name="" id="" placeholder="start hour" class="form-control"> 
                                                </td>
                                                <td>
                                                   <input type="text" name="" id="" placeholder="finish hour" class="form-control"> 
                                                </td>
                                                <td>
                                                    <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2">
                                                    <a href="#" class="btn btn-primary btn-sm btn-block">
                                                        Add New 
                                                    </a>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>

                            <div class="box-footer">
                                <a class="btn btn-primary btn-sm" href="<?=base_url().getController()?>">
                                    <i class="fa fa-save"></i> Save
                                </a>
                                <a class="btn btn-dark btn-sm" href="<?=base_url().getController()?>">
                                    <i class="fa fa-trash"></i> Canccel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

</script>