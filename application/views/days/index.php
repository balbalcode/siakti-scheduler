
<div class="content-header">

</div>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Days College</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 pb-2">
                        <div class="float-right">
                            <a class="btn btn-primary btn-sm"  href="<?=base_url().getController()?>/form">
                                <i class="fa fa-plus"></i>  Add New Day
                            </a>
                        </div>
                        <div class="clearboth"></div>
                    </div>
                    <div class="col-md-12">
                        <table id="datatable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>NIP</th>
                                    <th>Name</th>
                                    <th>Amount of Hours</th>
                                    <th width="15%">Option</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{NIP}}</td>
                                    <td>Internet Explorer 4.0 </td>
                                    <td>10 Hours/Week</td>
                                    <td>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit 
                                        </a>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-danger btn-xs">
                                            <i class="fa fa-times"></i> Delete 
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{NIP}}</td>
                                    <td>Internet Explorer 5.0</td>
                                    <td>10 Hours/Week</td>
                                    <td>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit 
                                        </a>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-danger btn-xs">
                                            <i class="fa fa-times"></i> Delete 
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{NIP}}</td>
                                    <td>Internet Explorer 5.5 </td>
                                    <td>10 Hours/Week</td>
                                    <td>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit 
                                        </a>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-danger btn-xs">
                                            <i class="fa fa-times"></i> Delete 
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{NIP}}</td>
                                    <td>Internet Explorer 6 </td>
                                    <td>10 Hours/Week</td>
                                    <td>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit 
                                        </a>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-danger btn-xs">
                                            <i class="fa fa-times"></i> Delete 
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{NIP}}</td>
                                    <td>Internet Explorer 7</td>
                                    <td>10 Hours/Week</td>
                                    <td>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit 
                                        </a>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-danger btn-xs">
                                            <i class="fa fa-times"></i> Delete 
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{NIP}}</td>
                                    <td>AOL browser (AOL desktop)</td>
                                    <td>10 Hours/Week</td>
                                    <td>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit 
                                        </a>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-danger btn-xs">
                                            <i class="fa fa-times"></i> Delete 
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{NIP}}</td>
                                    <td>Firefox 1.0</td>
                                    <td>10 Hours/Week</td></td>
                                    <td>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit 
                                        </a>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-danger btn-xs">
                                            <i class="fa fa-times"></i> Delete 
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{NIP}}</td>
                                    <td>Firefox 1.5</td>
                                    <td>10 Hours/Week</td></td>
                                    <td>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit 
                                        </a>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-danger btn-xs">
                                            <i class="fa fa-times"></i> Delete 
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{NIP}}</td>
                                    <td>Firefox 2.0</td>
                                    <td>10 Hours/Week</td></td>
                                    <td>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit 
                                        </a>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-danger btn-xs">
                                            <i class="fa fa-times"></i> Delete 
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{NIP}}</td>
                                    <td>Firefox 3.0</td>
                                    <td>10 Hours/Week</td></td>
                                    <td>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit 
                                        </a>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-danger btn-xs">
                                            <i class="fa fa-times"></i> Delete 
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

</script>