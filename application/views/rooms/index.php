<div class="content-header">

</div>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Room</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 pb-2">
                        <div class="float-right">
                            <a class="btn btn-primary btn-sm" href="<?= base_url() . getController() ?>/form">
                                <i class="fa fa-plus"></i> Add New Rooms
                            </a>
                        </div>
                        <div class="clearboth"></div>
                    </div>
                    <div class="col-md-12">
                        <table id="datatable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Nama</th>
                                    <th>Lokasi</th>
                                    <th>Kapasitas</th>
                                    <th width="15%">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                if (@$data->data ) {
                                    foreach ($data->data as $key => $value) { ?>
                                        <tr>
                                            <td><?= $no ?></td>
                                            <td><?= $value->namaruang ?></td>
                                            <td><?= $value->lokasi_gedung ?></td>
                                            <td><?= $value->kapasitas ?></td>
                                            <td>
                                                <a href="<?= base_url() . getController() ?>/form/<?= $value->namaruang ?>" class="btn btn-primary btn-xs">
                                                    <i class="fa fa-edit"></i> Sunting
                                                </a>
                                                <button class="btn btn-danger btn-xs del-dialog" data-id="<?=$value->namaruang?>" data-route="<?=base_url().getController().'/delete/'.$value->namaruang?>" data-redirect="<?=base_url().getController()?>">
                                                    <i class="fa fa-times"></i> Hapus
                                                </button>
                                            </td>
                                        </tr>
                                        <?php $no++;
                                    } }?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>