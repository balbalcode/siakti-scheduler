
<div class="content-header">

</div>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Ruangan</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="mx-auto col-10 col-md-9">
                        <form role="form" action="<?=base_url()?>Rooms/save" method="post">
                            <div class="box-body">
                                <input type="hidden" name="oldName" value="<?=@$data[0]->namaruang?>">
                                <?=input_text_group("namaruang",@$data[0]->namaruang," Nama Ruangan" ,"Masukkan Nama")?>
                                <?php echo form_error('namaruang'); ?>
                                <?=input_text_group("lokasi_gedung",@$data[0]->lokasi_gedung,"Detail Location" ,"Masukkan ","true")?>
                                <?php echo form_error('lokasi_gedung'); ?>
                                <?=input_text_group("kapasitas",@$data[0]->kapasitas," Kapasitas" ,"Masukkan Kapasitas","true")?>
                                <?php echo form_error('kapasitas'); ?>

                            </div>
                            <div class="box-footer">
                                <button class="btn btn-primary btn-sm" type="submit">
                                    <i class="fa fa-save"></i> Simpan
                                </button>
                                <a class="btn btn-dark btn-sm" href="<?=base_url().getController()?>">
                                    <i class="fa fa-trash"></i> Batal
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>