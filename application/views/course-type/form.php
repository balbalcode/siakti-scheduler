
<div class="content-header">

</div>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Course Type</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="mx-auto col-10 col-md-9">
                        <form role="form">
                            <div class="box-body">
                                <?=input_text_group("courseType","","Course Type" ,"Insert Type","true")?>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <a class="btn btn-primary btn-sm" href="<?=base_url().getController()?>">
                                    <i class="fa fa-save"></i> Save
                                </a>
                                <a class="btn btn-dark btn-sm" href="<?=base_url().getController()?>">
                                    <i class="fa fa-trash"></i> Canccel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

</script>