
<div class="content-header">

</div>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Academic</h2>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 pb-2">
                        <div class="float-right">
                            <a class="btn btn-primary btn-sm"  href="<?=base_url().getController()?>/form">
                                <i class="fa fa-plus"></i>  Add New Academics
                            </a>
                        </div>
                        <div class="clearboth"></div>
                    </div>
                    <div class="col-md-12">
                        <table id="datatable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Course Type</th>
                                    <th width="15%">Option</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{NUM}}</td>
                                    <td>Non Produktif </td>
                                    <td>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit 
                                        </a>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-danger btn-xs">
                                            <i class="fa fa-times"></i> Delete 
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{NUM}}</td>
                                    <td>Produktif</td>
                                    <td>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit 
                                        </a>
                                        <a href="<?=base_url().getController()?>/form" class="btn btn-danger btn-xs">
                                            <i class="fa fa-times"></i> Delete 
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

</script>