<?php 
if (!function_exists('input_text')) {
	function input_text($name="",$value="",$placeholder="",$required="",$attr = array(),$length="",$type=""){
		if (!$type) {
			$type="text";
		}

		if (!$required) {
			$required=false;
		}

		if($length){
			$length = "maxlength='$length'";
		}

		$attribute = '';
		if(!empty($attr) OR is_array($attr))
		{
			foreach($attr as $key => $attrs)
			{
				$attribute .= ' '.$key.'="'.$attrs.'"';
			}
		}


		$result .=    "<input type='$type' class='form-control' id='$name' name='$name' value='$value' placeholder='$placeholder' required='$required' $length $attribute>";
		return $result;
	}

	function input_text_group($name="",$value="", $label='' ,$placeholder="",$required="",$attr = array(),$length="",$type=""){
		if (!$type) {
			$type="text";
		}

		if (!$required) {
			$required=false;
		}

		if($length){
			$length = "maxlength='$length'";
		}

		$attribute = '';
		if(!empty($attr) OR is_array($attr))
		{
			foreach($attr as $key => $attrs)
			{
				$attribute .= ' '.$key.'="'.$attrs.'"';
			}
		}


		$result = "<div class='form-group my-2'>";
		$result .=    "<label for='$name'>$label</label>";
		$result .=    "<input type='$type' class='form-control' id='$name' name='$name' value='$value' placeholder='$placeholder' $required $length $attribute>";
		$result .= "</div>";
		return $result;
	}

	function input_textarea($name="",$value="",$placeholder="",$required="",$attr = array(),$length=""){
		

		if (!$required) {
			$required=false;
		}

		if($length){
			$length = "maxlength='$length'";
		}

		$attribute = '';
		if(!empty($attr) OR is_array($attr))
		{
			foreach($attr as $key => $attrs)
			{
				$attribute .= ' '.$key.'="'.$attrs.'"';
			}
		}


		$result .=    "<textarea class='form-control' id='$name' name='$name' placeholder='$placeholder' required='$required' $length $attribute>$value</textarea>";
		return $result;
	}

	function input_textarea_group($name="",$value="", $label='' ,$placeholder="",$required="",$attr = array(),$length=""){
		
		if (!$required) {
			$required=false;
		}

		if($length){
			$length = "maxlength='$length'";
		}

		$attribute = '';
		if(!empty($attr) OR is_array($attr))
		{
			foreach($attr as $key => $attrs)
			{
				$attribute .= ' '.$key.'="'.$attrs.'"';
			}
		}


		$result = "<div class='form-group my-2'>";
		$result .=    "<label for='$name'>$label</label>";
		$result .=    "<textarea class='form-control' id='$name' name='$name' placeholder='$placeholder' required='$required' $length $attribute>$value</textarea>";
		$result .= "</div>";
		return $result;
	}

	function select_join($data="", $params="", $show="", $name="", $attr=array(), $required="", $disabled="", $placeholder="", $width="")
	{
		$attribute = '';
		if(!empty($attr) OR is_array($attr))
		{
			foreach($attr as $key => $attrs)
			{
				$attribute .= ' '.$key.'="'.$attrs.'"';
			}
		}
		$select  = "<select class='form-control search-select' '".$required."' data-placeholder='".$placeholder."' name='".$name."' id='".$name."' style='color:black;width:{$width};' ".$attribute." data-validation='".$required."' data-validation-error-msg='Anda belum mengisi field ini' $disabled>";
		$select .= "<option selected disabled>Select Data</option>";

		foreach ($data as $key => $value) {
			if($value[$name]==$params)
			{
				$select .= "<option value='".$value[$name]."' selected>".$value[$show]."</option>";
			}
			else {
				$select .= "<option value='".$value[$name]."'>".$value[$show]."</option>";

			}
		}

		$select .= "</select>";

		return $select;
	}

	function select_join_group($data="", $params="", $show="", $name="", $label='', $attr=array(), $required="", $disabled="", $placeholder="", $width="" ){
		
		$attribute = '';
		if(!empty($attr) OR is_array($attr))
		{
			foreach($attr as $key => $attrs)
			{
				$attribute .= ' '.$key.'="'.$attrs.'"';
			}
		}

		$select = "<div class='form-group my-2'>";
		$select .=    "<label for='".$name."'>".$label."</label>";
		$select .= "<select class='form-control search-select' '".$required."' data-placeholder='".$placeholder."' name='".$name."' id='".$name."' style='color:black;width:{$width};' ".$attribute." data-validation='".$required."' data-validation-error-msg='Anda belum mengisi field ini' $disabled>";
		$select .= "<option selected disabled>Select Data</option>";
		$data = json_decode(json_encode($data), true);
		foreach ($data as $key => $value) {
			if($value[$name]==$params)
			{
				$select .= "<option value='".$value[$name]."' selected>".$value[$show]."</option>";
			}
			else {
				$select .= "<option value='".$value[$name]."'>".$value[$show]."</option>";

			}
		}

		$select .= "</select>";

		return $select;
	}

	

}

?>