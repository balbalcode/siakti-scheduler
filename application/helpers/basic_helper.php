<?php
if (!function_exists('getModule')) {
	function getModule()
	{
		$CI = get_instance();
		$query = $CI->router->fetch_module();
		return $query;
	}
}

if (!function_exists('getController')) {
	function getController()
	{
		$CI = get_instance();
		$query = $CI->router->class;
		return $query;
	}
}

if (!function_exists('getFunction')) {
	function getFunction()
	{
		$CI = get_instance();
		$query = $CI->router->method;
		return $query;
	}
}
