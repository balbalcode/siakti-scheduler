<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CourseType extends CI_Controller {

	public function index()
	{
		$data['title'] = 'AdminLTE 3 | CourseType';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('course-type/index','',true);
        $this->load->view('master',array('main'=>$data));
	}

	public function form($value='')
	{
		$data['title'] = 'AdminLTE 3 | CourseType';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('course-type/form','',true);
        $this->load->view('master',array('main'=>$data));
	}
}
