<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Days extends CI_Controller {

	public function index()
	{
		$data['title'] = 'AdminLTE 3 | Days';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('days/index','',true);
        $this->load->view('master',array('main'=>$data));
	}

	public function form()
	{
		$data['title'] = 'AdminLTE 3 | Days';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('days/form','',true);
        $this->load->view('master',array('main'=>$data));
    }
}
