<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Academics extends CI_Controller {

	public function index()
	{
		$pages['data'] = $this->retreiveData();
		$data['title'] = 'AdminLTE 3 | Semester';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('academics/index',$pages,true);
        $this->load->view('master',array('main'=>$data));
	}

	public function form($semester_nm='')
	{
		$pages = array();
		if($semester_nm){
			$pages['data'] = $this->retreiveData('?semester_nm='.$semester_nm)->responseData;
		}
		$data['title'] = 'AdminLTE 3 | Semester';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('academics/form',$pages,true);
        $this->load->view('master',array('main'=>$data));
	}

	public function retreiveData($semester_nm='')
	{
		$retr  = $this->customguzzle->getPlain('Semester'.$semester_nm,'application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function delete($data)
	{
		$data = array('semester_nm' => $data);
		$retr  = $this->customguzzle->delBlank('Semester','application/x-www-form-urlencoded', $data);	

	}

	public function save()
	{
		$post = $this->input->post();
		$this->form_validation->set_rules('semester_nm', 'Nama Semester', 'required|max_length[1]');
		if($this->form_validation->run() != false){
			if(!$post['oldJns']){
				$retr = $this->customguzzle->postBlank('Semester','application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('academics?inp=success');
				}
				else{
					redirect('academics?inp=fail');
				}
			}
			else{
				$retr = $this->customguzzle->putBlank('Semester/'.$post['oldJns'],'application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('academics?upd=success');
				}
				else{
					redirect('academics?upd=fail');
				}
			}
		}else{
			redirect('academics/form');
		}
	}
}
