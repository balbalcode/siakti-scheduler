<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends CI_Controller {

	public function index()
	{
		$data['title'] = 'AdminLTE 3 | Course';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('course/index','',true);
        $this->load->view('master',array('main'=>$data));
	}

	public function form($value='')
	{
		$data['title'] = 'AdminLTE 3 | Course';
		$pages['courseType'] = $this->getCourseType();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('course/form','',true);
        $this->load->view('master',array('main'=>$data));
	}

	public function getCourseType()
	{
		$data = array(
			0 => array("courseTypeId" => "1", "courseTypeName" => "Produktif"),
			1 => array("courseTypeId" => "2", "courseTypeName" => "Non Produktif")
		);

		return $data;
	}
}
