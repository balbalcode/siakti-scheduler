<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends CI_Controller {

	public function index()
	{
		$data['title'] = 'AdminLTE 3 | Course';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('course/index','',true);
        $this->load->view('master',array('main'=>$data));
	}

	public function form($value='')
	{
		$data['title'] = 'AdminLTE 3 | Course';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('course/form','',true);
        $this->load->view('master',array('main'=>$data));
    }
    
    public function getClass()
	{
		$data = array(
			0 => array("classId" => "1", "className" => "TI"),
			1 => array("classId" => "2", "className" => "TMD"),
			2 => array("classId" => "3", "className" => "TKJ"),
			3 => array("classId" => "4", "className" => "TMJ")	
		);

		return $data;
    }
    
    public function getMatkul()
	{
		$data = array(
			0 => array("matkulId" => "1", "matkulName" => "WEB 3"),
			1 => array("matkulId" => "2", "matkulName" => "WEB 3"),
			2 => array("matkulId" => "3", "matkulName" => "WEB 3"),
			3 => array("matkulId" => "4", "matkulName" => "WEB 3")	
		);

		return $data;
	}
}
