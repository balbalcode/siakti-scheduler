<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rooms extends CI_Controller {

	public function index()
	{
		$pages['data'] = $this->retreiveData();
		$data['title'] = 'AdminLTE 3 | Ruangan';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('rooms/index',$pages,true);
        $this->load->view('master',array('main'=>$data));
	}

	public function form($namaruang='')
	{
		$pages = array();
		if($namaruang){
			$pages['data'] = $this->retreiveData($namaruang)->data;
		}
		$data['title'] = 'AdminLTE 3 | Ruangan';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('rooms/form',$pages,true);
        $this->load->view('master',array('main'=>$data));
	}

	public function retreiveData($namaruang='')
	{
		$retr  = $this->customguzzle->getPlain('ruangan/'.$namaruang,'application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function delete($data)
	{
		$retr  = $this->customguzzle->delBlank('ruangan/'.$data);		
	}

	public function save()
	{
		$post = $this->input->post();
		$this->form_validation->set_rules('namaruang', 'Nama Ruangan', 'required');
		$this->form_validation->set_rules('lokasi_gedung', 'Lokasi Gedung', 'required');
		$this->form_validation->set_rules('kapasitas', 'Kapasitas', 'required|integer');
		if($this->form_validation->run() != false){
			if(!$post['oldName']){
				$post['status'] = 1;
				$retr = $this->customguzzle->postBlank('ruangan','application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('rooms?inp=success');
				}
				else{
					redirect('rooms?inp=fail');
				}
			}
			else{
				$post['status'] = 1;
				$retr = $this->customguzzle->putBlank('ruangan/'.$post['oldName'],'application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('rooms?upd=success');
				}
				else{
					redirect('rooms?upd=fail');
				}
			}
		}else{
			redirect('rooms/form');
		}
	}
}
