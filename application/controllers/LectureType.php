<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LectureType extends CI_Controller {

	public function index()
	{
		$data['title'] = 'AdminLTE 3 | Lecture Type';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('lecture-type/index','',true);
        $this->load->view('master',array('main'=>$data));
	}

	public function form()
	{
		$data['title'] = 'AdminLTE 3 | Lecture Type';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('lecture-type/form','',true);
        $this->load->view('master',array('main'=>$data));
    }
}
