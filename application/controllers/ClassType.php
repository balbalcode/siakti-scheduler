<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClassType extends CI_Controller {

	public function index()
	{
		$pages['data'] = $this->retreiveData();
		$data['title'] = 'AdminLTE 3 | Tipe Kelas';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
		$data['pages'] = $this->load->view('class-type/index',$pages,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form($nama_jnskls='')
	{
		$pages = array();
		if($nama_jnskls){
			$pages['data'] = $this->retreiveData('?nama_jnskls='.$nama_jnskls)->responseData;
		}
		$data['title'] = 'AdminLTE 3 | Tipe Kelas';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
		$data['pages'] = $this->load->view('class-type/form',$pages,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function retreiveData($nama_jnskls='')
	{
		$retr  = $this->customguzzle->getPlain('JenisKelas'.$nama_jnskls,'application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function delete($data)
	{
		$data = array('nama_jnskls' => $data);
		$retr  = $this->customguzzle->delBlank('JenisKelas','application/x-www-form-urlencoded', $data);	

	}

	public function save()
	{
		$post = $this->input->post();
		$this->form_validation->set_rules('nama_jnskls', 'Nama Jenis Kelas', 'required');
		if($this->form_validation->run() != false){
			if(!$post['oldJns']){
				$retr = $this->customguzzle->postBlank('JenisKelas','application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('classtype?inp=success');
				}
				else{
					redirect('classtype?inp=fail');
				}
			}
			else{
				$retr = $this->customguzzle->putBlank('JenisKelas/'.$post['oldJns'],'application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('classtype?upd=success');
				}
				else{
					redirect('classtype?upd=fail');
				}
			}
		}else{
			redirect('classtype/form');
		}
	}
}
