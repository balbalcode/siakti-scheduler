<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AcademicsYear extends CI_Controller {

	public function index()
	{
		$pages['data'] = $this->retreiveData();
		$data['title'] = 'AdminLTE 3 | Tahun Akademik';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('academics-year/index',$pages,true);
        $this->load->view('master',array('main'=>$data));
	}

	public function form($thn_akad_id='')
	{
		$pages = array();
		$pages['semesterList'] = $this->retreiveDropdownSemester();
		if($thn_akad_id){
			$pages['data'] = $this->retreiveData('?thn_akad_id='.$thn_akad_id)->responseData;
		}
		$data['title'] = 'AdminLTE 3 | Tahun Akademik';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('academics-year/form',$pages,true);
        $this->load->view('master',array('main'=>$data));
	}

	public function retreiveData($thn_akad_id='')
	{
		$retr  = $this->customguzzle->getPlain('TahunAkad'.$thn_akad_id,'application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function retreiveDropdownSemester()
	{
		$retr  = $this->customguzzle->getPlain('Semester','application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function delete($data)
	{
		$data = array('thn_akad_id' => $data);
		$retr  = $this->customguzzle->delBlank('TahunAkad','application/x-www-form-urlencoded', $data);	

	}

	public function save()
	{
		$post = $this->input->post();
		$this->form_validation->set_rules('thn_akad_id', 'Tahun Akademik', 'required');
		$this->form_validation->set_rules('tahun_akad', 'Tahun Kademik', 'required');
		$this->form_validation->set_rules('semester_nm', 'Semester', 'required');
		if($this->form_validation->run() != false){
			$post['semester_semester_nm'] = $post['semester_nm'];
			if($post['oldId'] == ''){
				$retr = $this->customguzzle->postBlank('TahunAkad','application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('academicsyear?upd=success');
				}
				else{
					redirect('academicsyear?upd=fail');
				}
			}
			else{
				$id = $post['oldId'];
				$retr = $this->customguzzle->putBlank('TahunAkad/'.$id,'application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('academicsyear?inp=success');
				}
				else{
					redirect('academicsyear?inp=fail');
				}
			}
		}else{
			echo validation_errors();die();
			redirect('academicsyear/form');
		}
	}
}
