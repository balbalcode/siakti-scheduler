<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curriculum extends CI_Controller {

	public function index()
	{
		$data['title'] = 'AdminLTE 3 | Kurikulum';
		$pages['data'] = $this->retreiveData();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('curriculum/index',$pages,true);
        $this->load->view('master',array('main'=>$data));
	}

	public function form($namakur='')
	{
		$pages = array();
		$pages['tahunAkadList'] = $this->retreiveDropdownThnAkad();
		$pages['prodiList'] = $this->retreiveDropdownProdi();
		if($namakur){
			$pages['data'] = $this->retreiveData('?namakur='.$namakur)->responseData;
		}
		$data['title'] = 'AdminLTE 3 | Kurikulum';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('curriculum/form',$pages,true);
        $this->load->view('master',array('main'=>$data));
	}

	public function retreiveData($namakur='')
	{
		$retr  = $this->customguzzle->getPlain('Kurikulum'.$namakur,'application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function retreiveDropdownThnAkad()
	{
		$retr  = $this->customguzzle->getPlain('TahunAkad','application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function retreiveDropdownProdi()
	{
		$retr  = $this->customguzzle->getPlain('Prodi','application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function delete($data)
	{
		$data = array('namakur' => $data);
		$retr  = $this->customguzzle->delBlank('Kurikulum','application/x-www-form-urlencoded', $data);	

	}

	public function save()
	{

		$post = $this->input->post();
		$this->form_validation->set_rules('namakur', 'Nama Kurikulum', 'required');
		$this->form_validation->set_rules('tgl_berlaku', 'Nama Kurikulum', 'required');
		$this->form_validation->set_rules('learn_out_prodi', 'Jenis Kurikulum', 'required');
		$this->form_validation->set_rules('prodi_id', 'Prodi', 'required'); 
		$this->form_validation->set_rules('thn_akad_id', 'Tahun Akademik', 'required'); 
		if($this->form_validation->run() != false){
			$post['thn_akad_thn_akad_id'] = $post['thn_akad_id'];
			$post['prodi_prodi_id'] = $post['prodi_id'];
			if($post['oldKode'] == ''){
				$retr = $this->customguzzle->postBlank('Kurikulum','application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('curriculum?upd=success');
				}
				else{
					redirect('curriculum?upd=fail');
				}
			}
			else{
				$id = $post['oldKode'];
				$retr = $this->customguzzle->putBlank('Kurikulum/'.$id,'application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('curriculum?inp=success');
				}
				else{
					redirect('curriculum?inp=fail');
				}
			}
		}else{
			redirect('curriculum/form');
		}
	}
}
