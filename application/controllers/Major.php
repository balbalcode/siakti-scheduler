<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Major extends CI_Controller {

	public function index()
	{
		$data['title'] = 'AdminLTE 3 | Jurusan';
		$pages['data'] = $this->retreiveData();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('major/index',$pages,true);
        $this->load->view('master',array('main'=>$data));
	}

	public function form($kodejur='')
	{
		$pages = array();
		if($kodejur){
			$pages['data'] = $this->retreiveData('?kodejur='.$kodejur)->responseData;
		}
		$data['title'] = 'AdminLTE 3 | Jurusan';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('major/form',$pages,true);
        $this->load->view('master',array('main'=>$data));
	}

	public function retreiveData($kodejur='')
	{
		$retr  = $this->customguzzle->getPlain('Jurusan'.$kodejur,'application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function delete($data)
	{
		$data = array('kodejur' => $data);
		$retr  = $this->customguzzle->delBlank('Jurusan','application/x-www-form-urlencoded', $data);	

	}

	public function save()
	{
		$post = $this->input->post();
		$this->form_validation->set_rules('kodejur', 'Major Code', 'required|integer');
		$this->form_validation->set_rules('namajur', 'Major Name', 'required');
		if($this->form_validation->run() != false){
			if(!$post['oldJur']){
				$retr = $this->customguzzle->postBlank('Jurusan','application/json', $post);

				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('major?inp=success');
				}
				else{
					redirect('major?inp=fail');
				}
			}
			else{
				$retr = $this->customguzzle->putBlank('Jurusan/'.$post['kodejur'],'application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('major?upd=success');
				}
				else{
					redirect('major?upd=fail');
				}
			}
		}else{
			redirect('major/form');
		}
	}
}
