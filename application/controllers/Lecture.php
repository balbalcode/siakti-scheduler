<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lecture extends CI_Controller {

	public function index()
	{
		$data['title'] = 'AdminLTE 3 | Lecture';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('lecture/index','',true);
        $this->load->view('master',array('main'=>$data));
	}

	public function form($value='')
	{
		$data['title'] = 'AdminLTE 3 | Lecture';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('lecture/form','',true);
        $this->load->view('master',array('main'=>$data));
	}
}
