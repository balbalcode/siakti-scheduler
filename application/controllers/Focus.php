<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Focus extends CI_Controller {

	public function index()
	{
		$data['title'] = 'AdminLTE 3 | Prodi';
		$pages['data'] = $this->retreiveData();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
		$data['pages'] = $this->load->view('major-focus/index',$pages,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function form($prodi_id='')
	{
		$pages = array();
		$pages['jurusanList'] = $this->retreiveDropdownJurusan();
		if($prodi_id){
			$pages['data'] = $this->retreiveData('?prodi_id='.$prodi_id)->responseData;
		}
		$data['title'] = 'AdminLTE 3 | Prodi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
		$data['pages'] = $this->load->view('major-focus/form',$pages,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function retreiveData($prodi_id='')
	{
		$retr  = $this->customguzzle->getPlain('Prodi'.$prodi_id,'application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function retreiveDropdownJurusan()
	{
		$retr  = $this->customguzzle->getPlain('Jurusan','application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function delete($data)
	{
		$data = array('prodi_id' => $data);
		$retr  = $this->customguzzle->delBlank('Prodi','application/x-www-form-urlencoded', $data);	

	}

	public function save()
	{
		$post = $this->input->post();
		$this->form_validation->set_rules('namaprod', 'Focus Name', 'required');
		$this->form_validation->set_rules('jenprod', 'Type Name', 'required|max_length[2]');
		$this->form_validation->set_rules('kodejur', 'Major', 'required');
		if($this->form_validation->run() != false){
			
			$post['jurusan_kodejur'] = $post['kodejur'];
			unset($post['kodejur']); 
			if($post['prodi_id'] != ''){
				$retr = $this->customguzzle->postBlank('Prodi','application/json', $post);

				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('focus?inp=success');
				}
				else{
					redirect('focus?inp=fail');
				}
			}
			else{
				$post['prodi_id'] = substr(rand(),0, 1);
				$retr = $this->customguzzle->putBlank('Prodi/'.$post['prodi_id'],'application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('focus?upd=success');
				}
				else{
					redirect('focus?upd=fail');
				}
			}
		}else{
			redirect('focus/form');
		}
	}
}
