<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends CI_Controller {

	public function index()
	{
		$data['title'] = 'AdminLTE 3 | Kelas';
		$pages['data'] = $this->retreiveData();
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('class/index',$pages,true);
        $this->load->view('master',array('main'=>$data));
	}

	public function form($kode_kelas='')
	{
		$pages = array();
		$pages['kelasList'] = $this->retreiveDropdownKelas();
		$pages['prodiList'] = $this->retreiveDropdownProdi();
		if($kode_kelas){
			$pages['data'] = $this->retreiveData('?kode_kelas='.$kode_kelas)->responseData;
		}
		$data['title'] = 'AdminLTE 3 | Kelas';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('class/form',$pages,true);
        $this->load->view('master',array('main'=>$data));
	}

	public function retreiveData($kode_kelas='')
	{
		$retr  = $this->customguzzle->getPlain('Kelas'.$kode_kelas,'application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function retreiveDropdownKelas()
	{
		$retr  = $this->customguzzle->getPlain('JenisKelas','application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function retreiveDropdownProdi()
	{
		$retr  = $this->customguzzle->getPlain('Prodi','application/json');
		if (@$retr['status'] =='401') {
			$retr = array();
		}
		else{
			$retr = json_decode($retr);
		}
		return $retr;
	}

	public function delete($data)
	{
		$data = array('kodeklas' => $data);
		$retr  = $this->customguzzle->delBlank('Kelas','application/x-www-form-urlencoded', $data);	

	}

	public function save()
	{
		$post = $this->input->post();
		$this->form_validation->set_rules('kodeklas', 'Kode Kelas', 'required');
		$this->form_validation->set_rules('namaklas', 'Nama Kelas', 'required|max_length[7]');
		$this->form_validation->set_rules('nama_jnskls', 'Jenis Kelas', 'required');
		$this->form_validation->set_rules('prodi_id', 'Prodi', 'required'); 
		if($this->form_validation->run() != false){
			$post['jns_kls_nama_jnskls'] = $post['nama_jnskls'];
			$post['prodi_prodi_id'] = $post['prodi_id'];
			if($post['oldKode'] == ''){
				$retr = $this->customguzzle->postBlank('Kelas','application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('classes?upd=success');
				}
				else{
					redirect('classes?upd=fail');
				}
			}
			else{
				$id = $post['oldKode'];
				$retr = $this->customguzzle->putBlank('Kelas/'.$id,'application/json', $post);
				if(@$retr['status'] == 200 && $retr['data'] != ''){
					redirect('classes?inp=success');
				}
				else{
					redirect('classes?inp=fail');
				}
			}
		}else{
			redirect('classes/form');
		}
	}
}
